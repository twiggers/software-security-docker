<?php defined('IN_CMS') or die('No direct access allowed.');

/*
	TestCMS - Default configuration
*/
return array(
	//  MySQL database details
	'database' => array(
		'host' => $_ENV['MYSQL_PORT_3306_TCP_ADDR'],
		'username' => 'root',
		'password' => $_ENV['MYSQL_ENV_MYSQL_ROOT_PASSWORD'],
		'name' => $_ENV['MYSQL_ENV_MYSQL_DATABASE']
	),
	
	// Application settings
	'application' => array(
		// url paths
		'base_url' => '/',
		'index_page' => 'index.php',

		// your time zone
		'timezone' => 'UTC',

		// access to admin
		'admin_folder' => 'admin',

		// your unique application key used for signing passwords
		'key' => ''
	),
	
	// Session details
	'session' => array(
		'name' => 'testcms',
		'expire' => 3600,
		'path' => '/',
		'domain' => ''
	),

	// Error handling
	'error' => array(
		'ignore' => array(E_NOTICE, E_USER_NOTICE, E_DEPRECATED, E_USER_DEPRECATED),
		'detail' => true,
		'log' => true
	),

	// Show database profile
	'debug' => true
);
