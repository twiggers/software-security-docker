#!/bin/bash
docker run -i --link testcms-mysql:mysql --rm testcms /bin/sh -c '/usr/bin/mysql -h $MYSQL_PORT_3306_TCP_ADDR -p$MYSQL_ENV_MYSQL_ROOT_PASSWORD $MYSQL_ENV_MYSQL_DATABASE' < mysqldump.sql
