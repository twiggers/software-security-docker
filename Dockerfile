FROM php:5.6-apache
RUN apt-get update
RUN apt-get install -y mysql-client
COPY ./mysqldump.sql /tmp/mysqldump.sql
RUN docker-php-ext-install mysql pdo_mysql
COPY ./testcms-v2 /var/www/html/
RUN chmod -R 777 /var/www/html
