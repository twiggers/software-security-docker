#!/bin/bash
docker start testcms-mysql || docker run --name testcms-mysql -e MYSQL_ROOT_PASSWORD=testpw -e MYSQL_DATABASE=testcms -d mysql:latest
echo "If you get errors while running the webapp, try running initdb.sh"
echo "You can now connect to http://localhost/, user admin, pass password"
docker run -it --link testcms-mysql:mysql -p 80:80 --rm testcms 
